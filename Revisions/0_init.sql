--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE nodejs;
ALTER ROLE nodejs WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION PASSWORD 'md568c9c48ac906b15501e425d4f55fb119';
CREATE ROLE postgres;
ALTER ROLE postgres WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION;
CREATE ROLE sajay;
ALTER ROLE sajay WITH SUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION PASSWORD 'md51e81dc5008687c8374e79bb16ca1d94f';






--
-- Database creation
--

CREATE DATABASE jayvengames WITH TEMPLATE = template0 OWNER = postgres;
REVOKE ALL ON DATABASE jayvengames FROM PUBLIC;
REVOKE ALL ON DATABASE jayvengames FROM postgres;
GRANT ALL ON DATABASE jayvengames TO postgres;
GRANT CONNECT,TEMPORARY ON DATABASE jayvengames TO PUBLIC;
GRANT ALL ON DATABASE jayvengames TO sajay;
REVOKE ALL ON DATABASE template1 FROM PUBLIC;
REVOKE ALL ON DATABASE template1 FROM postgres;
GRANT ALL ON DATABASE template1 TO postgres;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;


\connect jayvengames

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: plpythonu; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpythonu WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpythonu; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpythonu IS 'PL/PythonU untrusted procedural language';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET search_path = public, pg_catalog;

--
-- Name: py_log(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: sajay
--

CREATE FUNCTION py_log(status integer, component integer, detail character varying) RETURNS void
    LANGUAGE plpythonu SECURITY DEFINER
    AS '
  get_detailCode = plpy.prepare("select code from lookup_detail where LOWER(subject) = LOWER($1)", ["text"]);
  detailCode = plpy.execute(get_detailCode, [detail]);
  if (len(detailCode) == 0):
    create_detailCode = plpy.prepare(''''''INSERT INTO lookup_detail 
      (code, subject) 
      VALUES
      (NEXTVAL(''profiles_id_seq''), $1)'''''',
      ["text"]
    )
    plpy.execute(create_detailCode, [detail])
    detailCode = plpy.execute(''''''select CURRVAL(''profiles_id_seq'')'''''')[0]["currval"]
  else:
    detailCode = detailCode[0]["code"]
  
  insert_plan = plpy.prepare(''''''INSERT INTO log_gen
    (status, component, detail, date_filed)
    values
    ($1, $2, $3, current_timestamp)
    '''''',
    ["int", "int", "int"]
  )
  plpy.execute(insert_plan, [status, component, detailCode]);
';


ALTER FUNCTION public.py_log(status integer, component integer, detail character varying) OWNER TO sajay;

--
-- Name: py_log_request(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: sajay
--

CREATE FUNCTION py_log_request(status integer, session integer, url character varying) RETURNS void
    LANGUAGE plpythonu SECURITY DEFINER
    AS '
  get_urlCode = plpy.prepare("select code from lookup_request where LOWER(subject) = LOWER($1)", ["text"]);
  urlCode = plpy.execute(get_urlCode, [url]);
  if (len(urlCode) == 0):
    create_urlCode = plpy.prepare(''''''INSERT INTO lookup_request 
      (code, subject) 
      VALUES
      (NEXTVAL(''profiles_id_seq''), $1)'''''',
      ["text"]
    )
    plpy.execute(create_urlCode, [url])
    urlCode = plpy.execute(''''''select CURRVAL(''profiles_id_seq'')'''''')[0]["currval"]
  else:
    urlCode = urlCode[0]["code"]
  
  insert_plan = plpy.prepare(''''''INSERT INTO log_req
    (status, sessionid, url, date_filed)
    values
    ($1, $2, $3, current_timestamp)
    '''''',
    ["int", "int", "int"]
  )
  if (session <= 0):
    plpy.execute(insert_plan, [status, None, urlCode]);
  else:
    plpy.execute(insert_plan, [status, session, urlCode]);
';


ALTER FUNCTION public.py_log_request(status integer, session integer, url character varying) OWNER TO sajay;

--
-- Name: py_save_get(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: sajay
--

CREATE FUNCTION py_save_get(userid integer, gameid integer, saveid integer) RETURNS text
    LANGUAGE plpythonu SECURITY DEFINER
    AS '
  select_plan = plpy.prepare(''''''SELECT detail FROM game_saves WHERE
    userid = $1 AND gameid = $2 AND saveid = $3;'''''', ["int","int","int"])

  saveDetail = plpy.execute(select_plan, [userid, gameid, saveid], 1)
  return saveDetail[0]["detail"];
';


ALTER FUNCTION public.py_save_get(userid integer, gameid integer, saveid integer) OWNER TO sajay;

--
-- Name: py_save_set(integer, integer, integer, text); Type: FUNCTION; Schema: public; Owner: sajay
--

CREATE FUNCTION py_save_set(userid integer, gameid integer, saveid integer, detail text) RETURNS void
    LANGUAGE plpythonu SECURITY DEFINER
    AS '
  from plpy import spiexceptions
  try:
    insert_plan = plpy.prepare(''''''insert into game_saves
      (userid, gameid, saveid, detail)
      VALUES
      ($1, $2, $3, $4)'''''',
      ["int","int","int","text"]
    )
    plpy.execute(insert_plan, [userid, gameid, saveid, detail])
  except spiexceptions.UniqueViolation:
    insert_plan = plpy.prepare(''''''update game_saves
      set detail = $4
      where 
        userid = $1 and
        gameid = $2 and
        saveid = $3'''''',
      ["int","int","int","text"]
    )
    plpy.execute(insert_plan, [userid, gameid, saveid, detail]);
';


ALTER FUNCTION public.py_save_set(userid integer, gameid integer, saveid integer, detail text) OWNER TO sajay;

--
-- Name: py_session_close(integer); Type: FUNCTION; Schema: public; Owner: sajay
--

CREATE FUNCTION py_session_close(sessionid integer) RETURNS void
    LANGUAGE plpythonu SECURITY DEFINER
    AS '
  select_plan = plpy.prepare("select userid, sessionid, ip, connect from sessions_current where sessionid = $1", ["int"])
  session_record = plpy.execute(select_plan, [sessionid])[0]
  delete_plan = plpy.prepare("delete from sessions_current where sessionid = $1", ["int"])
  plpy.execute(delete_plan, [sessionid])
  
  insert_plan = plpy.prepare(''''''
      INSERT INTO sessions_past
      (userid, sessionid, ip, connect, disconnect)
      VALUES
      ($1, $2, $3, $4, CURRENT_TIMESTAMP)''''''
    ,
    ["int", "int", "inet", "timestamp"]
  )
  plpy.execute(insert_plan, [session_record["userid"], session_record["sessionid"], session_record["ip"], session_record["connect"]]);
';


ALTER FUNCTION public.py_session_close(sessionid integer) OWNER TO sajay;

--
-- Name: py_session_create(integer, inet); Type: FUNCTION; Schema: public; Owner: sajay
--

CREATE FUNCTION py_session_create(userid integer, ip inet) RETURNS integer
    LANGUAGE plpythonu SECURITY DEFINER
    AS '
  insert_plan = plpy.prepare(''''''
      INSERT INTO sessions_current
      (userid, sessionid, ip, connect)
      VALUES
      ($1, NEXTVAL(''curr_sessions_sessionid_seq''),$2, CURRENT_TIMESTAMP)''''''
    ,
    ["int", "inet"]
  )
  plpy.execute(insert_plan, [userid, ip])
  
  return plpy.execute(''''''select CURRVAL(''curr_sessions_sessionid_seq'')'''''')[0]["currval"];
';


ALTER FUNCTION public.py_session_create(userid integer, ip inet) OWNER TO sajay;

--
-- Name: py_user_ban(integer); Type: FUNCTION; Schema: public; Owner: sajay
--

CREATE FUNCTION py_user_ban(userid integer) RETURNS void
    LANGUAGE plpythonu SECURITY DEFINER
    AS '
  ban_plan = plpy.prepare(''''''UPDATE profiles
    SET passhash = crypt(gen_salt(''bf'', 8), gen_salt(''bf'', 8))
    WHERE id = $1'''''', ["int"])
    
  plpy.execute(ban_plan, [userid]);
';


ALTER FUNCTION public.py_user_ban(userid integer) OWNER TO sajay;

--
-- Name: py_user_change_pass(integer, text); Type: FUNCTION; Schema: public; Owner: sajay
--

CREATE FUNCTION py_user_change_pass(userid integer, password text) RETURNS void
    LANGUAGE plpythonu SECURITY DEFINER
    AS '
  update_plan = plpy.prepare(''''''UPDATE profiles
    SET passhash = crypt($2, gen_salt(''bf'', 8))
    WHERE id = $1'''''', ["int","text"])
    
  plpy.execute(update_plan, [userid, password]);
';


ALTER FUNCTION public.py_user_change_pass(userid integer, password text) OWNER TO sajay;

--
-- Name: py_user_check(character, character); Type: FUNCTION; Schema: public; Owner: sajay
--

CREATE FUNCTION py_user_check(username character, password character) RETURNS integer
    LANGUAGE plpythonu SECURITY DEFINER
    AS '
  select_plan = plpy.prepare(
    ''''''select id from profiles where 
      username = $1 and
      passhash = crypt($2, passhash)'''''',
    ["text", "text"]
  )
  
  id = plpy.execute(select_plan, [username, password])
  if (len(id) > 0):
    return id[0]["id"]
  else:
    return 0;
';


ALTER FUNCTION public.py_user_check(username character, password character) OWNER TO sajay;

--
-- Name: py_user_new(character, character, character); Type: FUNCTION; Schema: public; Owner: sajay
--

CREATE FUNCTION py_user_new(username character, password character, email character) RETURNS integer
    LANGUAGE plpythonu SECURITY DEFINER
    AS '
  insert_plan = plpy.prepare(''''''INSERT INTO profiles
    (
      id,
      username,
      passhash,
      email,
      created,
      last_login
    )
    VALUES
    (
      $5,
      $1,
      crypt($2, $3),
      $4,
      current_timestamp,
      current_timestamp
    )'''''',
      ["text", "text", "text", "text", "int"]
    )
  
  next_key = plpy.execute("select NEXTVAL(''profiles_id_seq'')", 1)[0]["nextval"]
  salt = plpy.execute("select gen_salt(''bf'', 8)", 1)[0]["gen_salt"]
  
  plpy.execute(insert_plan, [username, password, salt, email, next_key])
  return next_key;
';


ALTER FUNCTION public.py_user_new(username character, password character, email character) OWNER TO sajay;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: sessions_current; Type: TABLE; Schema: public; Owner: sajay; Tablespace: 
--

CREATE TABLE sessions_current (
    userid integer,
    sessionid integer NOT NULL,
    ip inet NOT NULL,
    connect timestamp without time zone NOT NULL
);


ALTER TABLE public.sessions_current OWNER TO sajay;

--
-- Name: curr_sessions_sessionid_seq; Type: SEQUENCE; Schema: public; Owner: sajay
--

CREATE SEQUENCE curr_sessions_sessionid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.curr_sessions_sessionid_seq OWNER TO sajay;

--
-- Name: curr_sessions_sessionid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sajay
--

ALTER SEQUENCE curr_sessions_sessionid_seq OWNED BY sessions_current.sessionid;


--
-- Name: game_saves; Type: TABLE; Schema: public; Owner: sajay; Tablespace: 
--

CREATE TABLE game_saves (
    gameid integer NOT NULL,
    userid integer NOT NULL,
    saveid integer NOT NULL,
    detail text NOT NULL
);


ALTER TABLE public.game_saves OWNER TO sajay;

--
-- Name: game_tags; Type: TABLE; Schema: public; Owner: sajay; Tablespace: 
--

CREATE TABLE game_tags (
    gameid integer NOT NULL,
    tagid integer NOT NULL
);


ALTER TABLE public.game_tags OWNER TO sajay;

--
-- Name: game_themes; Type: TABLE; Schema: public; Owner: sajay; Tablespace: 
--

CREATE TABLE game_themes (
    gameid integer NOT NULL,
    themeid integer NOT NULL
);


ALTER TABLE public.game_themes OWNER TO sajay;

--
-- Name: games; Type: TABLE; Schema: public; Owner: sajay; Tablespace: 
--

CREATE TABLE games (
    id integer NOT NULL,
    name text NOT NULL,
    created timestamp without time zone NOT NULL,
    changed timestamp without time zone NOT NULL,
    template text NOT NULL,
    res_x integer,
    res_y integer,
    folder text,
    url text,
    mobile_url text,
    source text,
    gif text
);


ALTER TABLE public.games OWNER TO sajay;

--
-- Name: log_gen; Type: TABLE; Schema: public; Owner: sajay; Tablespace: 
--

CREATE TABLE log_gen (
    status integer NOT NULL,
    component integer NOT NULL,
    detail integer NOT NULL,
    date_filed timestamp without time zone NOT NULL
);


ALTER TABLE public.log_gen OWNER TO sajay;

--
-- Name: log_req; Type: TABLE; Schema: public; Owner: sajay; Tablespace: 
--

CREATE TABLE log_req (
    status integer NOT NULL,
    sessionid integer,
    url integer NOT NULL,
    date_filed timestamp without time zone NOT NULL
);


ALTER TABLE public.log_req OWNER TO sajay;

--
-- Name: lookup_component; Type: TABLE; Schema: public; Owner: sajay; Tablespace: 
--

CREATE TABLE lookup_component (
    code integer NOT NULL,
    subject character(32) NOT NULL
);


ALTER TABLE public.lookup_component OWNER TO sajay;

--
-- Name: lookup_detail; Type: TABLE; Schema: public; Owner: sajay; Tablespace: 
--

CREATE TABLE lookup_detail (
    code integer NOT NULL,
    subject character varying(2000) NOT NULL
);


ALTER TABLE public.lookup_detail OWNER TO sajay;

--
-- Name: lookup_request; Type: TABLE; Schema: public; Owner: sajay; Tablespace: 
--

CREATE TABLE lookup_request (
    code integer NOT NULL,
    subject character varying(2000)
);


ALTER TABLE public.lookup_request OWNER TO sajay;

--
-- Name: lookup_request_code_seq; Type: SEQUENCE; Schema: public; Owner: sajay
--

CREATE SEQUENCE lookup_request_code_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lookup_request_code_seq OWNER TO sajay;

--
-- Name: lookup_status; Type: TABLE; Schema: public; Owner: sajay; Tablespace: 
--

CREATE TABLE lookup_status (
    code integer NOT NULL,
    subject character(32)
);


ALTER TABLE public.lookup_status OWNER TO sajay;

--
-- Name: lookup_tag; Type: TABLE; Schema: public; Owner: sajay; Tablespace: 
--

CREATE TABLE lookup_tag (
    code integer NOT NULL,
    subject text NOT NULL
);


ALTER TABLE public.lookup_tag OWNER TO sajay;

--
-- Name: lookup_theme; Type: TABLE; Schema: public; Owner: sajay; Tablespace: 
--

CREATE TABLE lookup_theme (
    code integer NOT NULL,
    subject text NOT NULL
);


ALTER TABLE public.lookup_theme OWNER TO sajay;

--
-- Name: profiles; Type: TABLE; Schema: public; Owner: sajay; Tablespace: 
--

CREATE TABLE profiles (
    id integer NOT NULL,
    username character(32) NOT NULL,
    passhash character(64) NOT NULL,
    email character(254) NOT NULL,
    created timestamp without time zone NOT NULL,
    last_login timestamp without time zone NOT NULL
);


ALTER TABLE public.profiles OWNER TO sajay;

--
-- Name: profiles_id_seq; Type: SEQUENCE; Schema: public; Owner: sajay
--

CREATE SEQUENCE profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profiles_id_seq OWNER TO sajay;

--
-- Name: profiles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sajay
--

ALTER SEQUENCE profiles_id_seq OWNED BY profiles.id;


--
-- Name: sessions_past; Type: TABLE; Schema: public; Owner: sajay; Tablespace: 
--

CREATE TABLE sessions_past (
    userid integer,
    sessionid integer NOT NULL,
    ip inet NOT NULL,
    connect timestamp without time zone NOT NULL,
    disconnect timestamp without time zone NOT NULL
);


ALTER TABLE public.sessions_past OWNER TO sajay;

--
-- Name: view_all_sessions; Type: VIEW; Schema: public; Owner: sajay
--

CREATE VIEW view_all_sessions AS
    SELECT f.userid, f.sessionid, f.ip, f.connect FROM (SELECT sessions_current.userid, sessions_current.sessionid, sessions_current.ip, sessions_current.connect FROM sessions_current UNION SELECT sessions_past.userid, sessions_past.sessionid, sessions_past.ip, sessions_past.connect FROM sessions_past) f ORDER BY f.connect DESC;


ALTER TABLE public.view_all_sessions OWNER TO sajay;

--
-- Name: view_log; Type: VIEW; Schema: public; Owner: sajay
--

CREATE VIEW view_log AS
    SELECT lookup_status.subject AS status, lookup_component.subject AS component, lookup_detail.subject AS detail, log_gen.date_filed FROM (((log_gen LEFT JOIN lookup_status ON ((log_gen.status = lookup_status.code))) LEFT JOIN lookup_component ON ((log_gen.component = lookup_component.code))) LEFT JOIN lookup_detail ON ((log_gen.detail = lookup_detail.code))) ORDER BY log_gen.date_filed DESC;


ALTER TABLE public.view_log OWNER TO sajay;

--
-- Name: view_lookup_logs; Type: VIEW; Schema: public; Owner: sajay
--

CREATE VIEW view_lookup_logs AS
    SELECT a.code AS code_s, a.subject AS status, b.code AS code_c, b.subject AS component FROM (lookup_status a FULL JOIN lookup_component b ON ((a.code = b.code)));


ALTER TABLE public.view_lookup_logs OWNER TO sajay;

--
-- Name: view_requests; Type: VIEW; Schema: public; Owner: sajay
--

CREATE VIEW view_requests AS
    SELECT lookup_status.subject AS status, view_all_sessions.ip, lookup_request.subject AS url, log_req.date_filed AS date FROM (((log_req JOIN lookup_request ON ((log_req.url = lookup_request.code))) JOIN lookup_status ON ((log_req.status = lookup_status.code))) LEFT JOIN view_all_sessions ON ((view_all_sessions.sessionid = log_req.sessionid))) ORDER BY log_req.date_filed DESC;


ALTER TABLE public.view_requests OWNER TO sajay;

--
-- Name: view_tables_on_disc; Type: VIEW; Schema: public; Owner: sajay
--

CREATE VIEW view_tables_on_disc AS
    SELECT n.nspname AS schema, c.relname AS "table", c.reltuples AS row_estimate, pg_size_pretty(pg_total_relation_size((c.oid)::regclass)) AS total, c.oid FROM (pg_class c LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace))) WHERE (c.relkind = 'r'::"char") ORDER BY pg_total_relation_size((c.oid)::regclass) DESC;


ALTER TABLE public.view_tables_on_disc OWNER TO sajay;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: sajay
--

ALTER TABLE ONLY profiles ALTER COLUMN id SET DEFAULT nextval('profiles_id_seq'::regclass);


--
-- Name: sessionid; Type: DEFAULT; Schema: public; Owner: sajay
--

ALTER TABLE ONLY sessions_current ALTER COLUMN sessionid SET DEFAULT nextval('curr_sessions_sessionid_seq'::regclass);


--
-- Name: component_lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: sajay; Tablespace: 
--

ALTER TABLE ONLY lookup_component
    ADD CONSTRAINT component_lookup_pkey PRIMARY KEY (code);


--
-- Name: detail_lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: sajay; Tablespace: 
--

ALTER TABLE ONLY lookup_detail
    ADD CONSTRAINT detail_lookup_pkey PRIMARY KEY (code);


--
-- Name: game_saves_pkey; Type: CONSTRAINT; Schema: public; Owner: sajay; Tablespace: 
--

ALTER TABLE ONLY game_saves
    ADD CONSTRAINT game_saves_pkey PRIMARY KEY (gameid, userid, saveid);


--
-- Name: games_pkey; Type: CONSTRAINT; Schema: public; Owner: sajay; Tablespace: 
--

ALTER TABLE ONLY games
    ADD CONSTRAINT games_pkey PRIMARY KEY (id);


--
-- Name: lookup_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: sajay; Tablespace: 
--

ALTER TABLE ONLY lookup_tag
    ADD CONSTRAINT lookup_tag_pkey PRIMARY KEY (code);


--
-- Name: lookup_theme_pkey; Type: CONSTRAINT; Schema: public; Owner: sajay; Tablespace: 
--

ALTER TABLE ONLY lookup_theme
    ADD CONSTRAINT lookup_theme_pkey PRIMARY KEY (code);


--
-- Name: profiles_email_key; Type: CONSTRAINT; Schema: public; Owner: sajay; Tablespace: 
--

ALTER TABLE ONLY profiles
    ADD CONSTRAINT profiles_email_key UNIQUE (email);


--
-- Name: profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: sajay; Tablespace: 
--

ALTER TABLE ONLY profiles
    ADD CONSTRAINT profiles_pkey PRIMARY KEY (id);


--
-- Name: profiles_username_key; Type: CONSTRAINT; Schema: public; Owner: sajay; Tablespace: 
--

ALTER TABLE ONLY profiles
    ADD CONSTRAINT profiles_username_key UNIQUE (username);


--
-- Name: request_lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: sajay; Tablespace: 
--

ALTER TABLE ONLY lookup_request
    ADD CONSTRAINT request_lookup_pkey PRIMARY KEY (code);


--
-- Name: status_lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: sajay; Tablespace: 
--

ALTER TABLE ONLY lookup_status
    ADD CONSTRAINT status_lookup_pkey PRIMARY KEY (code);


--
-- Name: curr_sessions_ip_idx; Type: INDEX; Schema: public; Owner: sajay; Tablespace: 
--

CREATE INDEX curr_sessions_ip_idx ON sessions_current USING btree (ip);


--
-- Name: curr_sessions_sessionid_idx; Type: INDEX; Schema: public; Owner: sajay; Tablespace: 
--

CREATE INDEX curr_sessions_sessionid_idx ON sessions_current USING btree (sessionid);


--
-- Name: curr_sessions_userid_idx; Type: INDEX; Schema: public; Owner: sajay; Tablespace: 
--

CREATE INDEX curr_sessions_userid_idx ON sessions_current USING btree (userid) WHERE (userid IS NOT NULL);


--
-- Name: log_component_idx; Type: INDEX; Schema: public; Owner: sajay; Tablespace: 
--

CREATE INDEX log_component_idx ON log_gen USING btree (component);


--
-- Name: log_date_filed_idx; Type: INDEX; Schema: public; Owner: sajay; Tablespace: 
--

CREATE INDEX log_date_filed_idx ON log_gen USING btree (date_filed);


--
-- Name: log_status_idx; Type: INDEX; Schema: public; Owner: sajay; Tablespace: 
--

CREATE INDEX log_status_idx ON log_gen USING btree (status);


--
-- Name: past_sessions_ip_idx; Type: INDEX; Schema: public; Owner: sajay; Tablespace: 
--

CREATE INDEX past_sessions_ip_idx ON sessions_past USING btree (ip);


--
-- Name: past_sessions_userid_idx; Type: INDEX; Schema: public; Owner: sajay; Tablespace: 
--

CREATE INDEX past_sessions_userid_idx ON sessions_past USING btree (userid) WHERE (userid IS NOT NULL);


--
-- Name: curr_sessions_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sajay
--

ALTER TABLE ONLY sessions_current
    ADD CONSTRAINT curr_sessions_userid_fkey FOREIGN KEY (userid) REFERENCES profiles(id);


--
-- Name: game_saves_gameid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sajay
--

ALTER TABLE ONLY game_saves
    ADD CONSTRAINT game_saves_gameid_fkey FOREIGN KEY (gameid) REFERENCES games(id);


--
-- Name: game_saves_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sajay
--

ALTER TABLE ONLY game_saves
    ADD CONSTRAINT game_saves_userid_fkey FOREIGN KEY (userid) REFERENCES profiles(id);


--
-- Name: game_tags_gameid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sajay
--

ALTER TABLE ONLY game_tags
    ADD CONSTRAINT game_tags_gameid_fkey FOREIGN KEY (gameid) REFERENCES games(id);


--
-- Name: game_tags_tagid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sajay
--

ALTER TABLE ONLY game_tags
    ADD CONSTRAINT game_tags_tagid_fkey FOREIGN KEY (tagid) REFERENCES lookup_tag(code);


--
-- Name: game_themes_gameid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sajay
--

ALTER TABLE ONLY game_themes
    ADD CONSTRAINT game_themes_gameid_fkey FOREIGN KEY (gameid) REFERENCES games(id);


--
-- Name: game_themes_themeid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sajay
--

ALTER TABLE ONLY game_themes
    ADD CONSTRAINT game_themes_themeid_fkey FOREIGN KEY (themeid) REFERENCES lookup_theme(code);


--
-- Name: gen_log_component_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sajay
--

ALTER TABLE ONLY log_gen
    ADD CONSTRAINT gen_log_component_fkey FOREIGN KEY (component) REFERENCES lookup_component(code);


--
-- Name: gen_log_detail_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sajay
--

ALTER TABLE ONLY log_gen
    ADD CONSTRAINT gen_log_detail_fkey FOREIGN KEY (detail) REFERENCES lookup_detail(code);


--
-- Name: gen_log_status_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sajay
--

ALTER TABLE ONLY log_gen
    ADD CONSTRAINT gen_log_status_fkey FOREIGN KEY (status) REFERENCES lookup_status(code);


--
-- Name: past_sessions_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sajay
--

ALTER TABLE ONLY sessions_past
    ADD CONSTRAINT past_sessions_id_fkey FOREIGN KEY (userid) REFERENCES profiles(id);


--
-- Name: req_log_request_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sajay
--

ALTER TABLE ONLY log_req
    ADD CONSTRAINT req_log_request_fkey FOREIGN KEY (url) REFERENCES lookup_request(code);


--
-- Name: req_log_status_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sajay
--

ALTER TABLE ONLY log_req
    ADD CONSTRAINT req_log_status_fkey FOREIGN KEY (status) REFERENCES lookup_status(code);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: py_log_request(integer, integer, character varying); Type: ACL; Schema: public; Owner: sajay
--

REVOKE ALL ON FUNCTION py_log_request(status integer, session integer, url character varying) FROM PUBLIC;
REVOKE ALL ON FUNCTION py_log_request(status integer, session integer, url character varying) FROM sajay;
GRANT ALL ON FUNCTION py_log_request(status integer, session integer, url character varying) TO sajay;
GRANT ALL ON FUNCTION py_log_request(status integer, session integer, url character varying) TO PUBLIC;
GRANT ALL ON FUNCTION py_log_request(status integer, session integer, url character varying) TO nodejs;


--
-- Name: py_save_get(integer, integer, integer); Type: ACL; Schema: public; Owner: sajay
--

REVOKE ALL ON FUNCTION py_save_get(userid integer, gameid integer, saveid integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION py_save_get(userid integer, gameid integer, saveid integer) FROM sajay;
GRANT ALL ON FUNCTION py_save_get(userid integer, gameid integer, saveid integer) TO sajay;
GRANT ALL ON FUNCTION py_save_get(userid integer, gameid integer, saveid integer) TO PUBLIC;
GRANT ALL ON FUNCTION py_save_get(userid integer, gameid integer, saveid integer) TO nodejs;


--
-- Name: py_save_set(integer, integer, integer, text); Type: ACL; Schema: public; Owner: sajay
--

REVOKE ALL ON FUNCTION py_save_set(userid integer, gameid integer, saveid integer, detail text) FROM PUBLIC;
REVOKE ALL ON FUNCTION py_save_set(userid integer, gameid integer, saveid integer, detail text) FROM sajay;
GRANT ALL ON FUNCTION py_save_set(userid integer, gameid integer, saveid integer, detail text) TO sajay;
GRANT ALL ON FUNCTION py_save_set(userid integer, gameid integer, saveid integer, detail text) TO PUBLIC;
GRANT ALL ON FUNCTION py_save_set(userid integer, gameid integer, saveid integer, detail text) TO nodejs;


--
-- Name: py_session_close(integer); Type: ACL; Schema: public; Owner: sajay
--

REVOKE ALL ON FUNCTION py_session_close(sessionid integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION py_session_close(sessionid integer) FROM sajay;
GRANT ALL ON FUNCTION py_session_close(sessionid integer) TO sajay;
GRANT ALL ON FUNCTION py_session_close(sessionid integer) TO PUBLIC;
GRANT ALL ON FUNCTION py_session_close(sessionid integer) TO nodejs;


--
-- Name: py_session_create(integer, inet); Type: ACL; Schema: public; Owner: sajay
--

REVOKE ALL ON FUNCTION py_session_create(userid integer, ip inet) FROM PUBLIC;
REVOKE ALL ON FUNCTION py_session_create(userid integer, ip inet) FROM sajay;
GRANT ALL ON FUNCTION py_session_create(userid integer, ip inet) TO sajay;
GRANT ALL ON FUNCTION py_session_create(userid integer, ip inet) TO PUBLIC;
GRANT ALL ON FUNCTION py_session_create(userid integer, ip inet) TO nodejs;


--
-- Name: py_user_ban(integer); Type: ACL; Schema: public; Owner: sajay
--

REVOKE ALL ON FUNCTION py_user_ban(userid integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION py_user_ban(userid integer) FROM sajay;
GRANT ALL ON FUNCTION py_user_ban(userid integer) TO sajay;
GRANT ALL ON FUNCTION py_user_ban(userid integer) TO PUBLIC;
GRANT ALL ON FUNCTION py_user_ban(userid integer) TO nodejs;


--
-- Name: py_user_change_pass(integer, text); Type: ACL; Schema: public; Owner: sajay
--

REVOKE ALL ON FUNCTION py_user_change_pass(userid integer, password text) FROM PUBLIC;
REVOKE ALL ON FUNCTION py_user_change_pass(userid integer, password text) FROM sajay;
GRANT ALL ON FUNCTION py_user_change_pass(userid integer, password text) TO sajay;
GRANT ALL ON FUNCTION py_user_change_pass(userid integer, password text) TO PUBLIC;
GRANT ALL ON FUNCTION py_user_change_pass(userid integer, password text) TO nodejs;


--
-- Name: py_user_check(character, character); Type: ACL; Schema: public; Owner: sajay
--

REVOKE ALL ON FUNCTION py_user_check(username character, password character) FROM PUBLIC;
REVOKE ALL ON FUNCTION py_user_check(username character, password character) FROM sajay;
GRANT ALL ON FUNCTION py_user_check(username character, password character) TO sajay;
GRANT ALL ON FUNCTION py_user_check(username character, password character) TO PUBLIC;
GRANT ALL ON FUNCTION py_user_check(username character, password character) TO nodejs;


--
-- Name: py_user_new(character, character, character); Type: ACL; Schema: public; Owner: sajay
--

REVOKE ALL ON FUNCTION py_user_new(username character, password character, email character) FROM PUBLIC;
REVOKE ALL ON FUNCTION py_user_new(username character, password character, email character) FROM sajay;
GRANT ALL ON FUNCTION py_user_new(username character, password character, email character) TO sajay;
GRANT ALL ON FUNCTION py_user_new(username character, password character, email character) TO PUBLIC;
GRANT ALL ON FUNCTION py_user_new(username character, password character, email character) TO nodejs;


--
-- PostgreSQL database dump complete
--

\connect postgres

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

\connect template1

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: template1; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

