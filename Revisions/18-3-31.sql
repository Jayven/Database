alter table sessions_current add column sess_hash char(64);
alter table sessions_current add column last_checkin timestamp;

drop function py_session_create(int, inet);
CREATE OR REPLACE FUNCTION public.py_session_create(userid integer, ip inet)
  RETURNS text
  LANGUAGE plpythonu
AS
$body$

  newkey = plpy.execute('''select NEXTVAL('curr_sessions_sessionid_seq')''')[0]["nextval"];
  
  identifier = plpy.execute('''select * from gen_salt('bf', 8)''')[0]["gen_salt"];
  
  gen_hash_plan = plpy.prepare('''
    select * from crypt($1, gen_salt('bf', 8));
    ''',
    ["text"]
  )
  
  hash = plpy.execute(gen_hash_plan, [identifier])[0]["crypt"];

  insert_plan = plpy.prepare('''
      INSERT INTO sessions_current
      (userid, sessionid, ip, connect, sess_hash, last_checkin)
      VALUES
      ($1, $2, $3, CURRENT_TIMESTAMP, $4, CURRENT_TIMESTAMP)'''
    ,
    ["int", "int", "inet", "text"]
  )
  
  plpy.execute(insert_plan, [userid, newkey, ip, hash])
  
  return identifier;
$body$
  VOLATILE
 SECURITY DEFINER
  COST 100;
  
CREATE OR REPLACE FUNCTION public.py_session_prune()
  RETURNS void
  LANGUAGE plpythonu
AS
$body$
  for row in plpy.cursor("select * from sessions_current where last_checkin < current_timestamp - time '00:30';"):
    delete_call_plan = plpy.prepare('''select * from py_session_close($1)''', ["int"]);
    plpy.execute(delete_call_plan, [row["sessionid"]]);
    
$body$
  VOLATILE
 SECURITY DEFINER
  COST 100;
  
CREATE OR REPLACE FUNCTION public.py_session_update(sess_key text)
  RETURNS void
  LANGUAGE plpythonu
AS
$body$
  update_plan = plpy.prepare('''
  update sessions_current
    set last_checkin = current_timestamp
    where sess_hash = crypt($1, sess_hash);
    ''',
    ["text"]
  );
  plpy.execute(update_plan, [sess_key]);
    
$body$
  VOLATILE
 SECURITY DEFINER
  COST 100;

CREATE OR REPLACE FUNCTION public.py_log_request(status integer, session_key text, url character varying)
  RETURNS void
  LANGUAGE plpythonu
AS
$body$
get_urlCode = plpy.prepare("select code from lookup_request where LOWER(subject) = LOWER($1)", ["text"]);
  
  urlCode = plpy.execute(get_urlCode, [url]);
  
  if (len(urlCode) == 0):
    create_urlCode = plpy.prepare('''INSERT INTO lookup_request 
      (code, subject) 
      VALUES
      (NEXTVAL('profiles_id_seq'), $1)''',
      ["text"]
    )
    plpy.execute(create_urlCode, [url])
    urlCode = plpy.execute('''select CURRVAL('profiles_id_seq')''')[0]["currval"]
  else:
    urlCode = urlCode[0]["code"]
  
  get_sessid_plan = plpy.prepare('''
    select sessionid from sessions_current where
       sess_hash = crypt($1, sess_hash);
    ''',
    ["text"]
  )
  sess_id = plpy.execute(get_sessid_plan, [session_key])[0]["sessionid"]
  
  insert_plan = plpy.prepare('''INSERT INTO log_req
    (status, sessionid, url, date_filed)
    values
    ($1, $2, $3, current_timestamp)
    ''',
    ["int", "int", "int"]
  )
  
  plpy.execute(insert_plan, [status, sess_id, urlCode]);
$body$
  VOLATILE
 SECURITY DEFINER
  COST 100;

CREATE OR REPLACE FUNCTION public.py_session_close(sessionid integer)
  RETURNS void
  LANGUAGE plpythonu
AS
$body$
  select_plan = plpy.prepare("select userid, sessionid, ip, connect, last_checkin from sessions_current where sessionid = $1", ["int"])
  session_record = plpy.execute(select_plan, [sessionid])[0]
  delete_plan = plpy.prepare("delete from sessions_current where sessionid = $1", ["int"])
  plpy.execute(delete_plan, [sessionid])
  
  insert_plan = plpy.prepare('''
      INSERT INTO sessions_past
      (userid, sessionid, ip, connect, disconnect)
      VALUES
      ($1, $2, $3, $4, $5)'''
    ,
    ["int", "int", "inet", "timestamp", "timestamp"]
  )
  plpy.execute(insert_plan, [session_record["userid"], session_record["sessionid"], session_record["ip"], session_record["connect"], session_record["last_checkin"]]);
$body$
  VOLATILE
 SECURITY DEFINER
  COST 100;
  
commit;
