CREATE OR REPLACE VIEW view_usage
(
  users,
  requests,
  hour,
  avg_duration
)
AS 
 SELECT count(a.requests) AS users,
    sum(a.requests) AS requests,
    min(date_trunc('hour'::text, a.open_date)) AS hour,
    avg(a.duration) AS avg_duration
   FROM ( SELECT req.requests,
            sess.last_updated - LEAST(req.open_date, sess.last_updated) AS duration,
            req.open_date
           FROM ( SELECT DISTINCT log_req.sessionid,
                    count(log_req.sessionid) AS requests,
                    min(log_req.date_filed) AS open_date
                   FROM log_req
                  GROUP BY log_req.sessionid
                  ORDER BY (min(log_req.date_filed)) DESC) req
             JOIN ( SELECT view_all_sessions.sessionid,
                    view_all_sessions.ip,
                    view_all_sessions.last_updated
                   FROM view_all_sessions where not ip = inet '::ffff:65.24.77.66' and not ip <<= inet '::ffff:192.168.0.0/112') sess ON req.sessionid = sess.sessionid
          WHERE req.requests > 1) a
  GROUP BY (date_trunc('hour'::text, a.open_date))
  ORDER BY (min(date_trunc('hour'::text, a.open_date))) DESC;


GRANT TRUNCATE, TRIGGER, DELETE, REFERENCES, SELECT, INSERT, UPDATE ON view_usage TO sajay;


COMMIT;

CREATE OR REPLACE FUNCTION public.py_query_username(username character)
  RETURNS bool
  LANGUAGE plpythonu
AS
$body$
select_plan = plpy.prepare('''select * from profiles where username = $1''', ["text"])
  
result = plpy.execute(select_plan, [username]);
  
return len(result) > 0;
$body$
  VOLATILE
 SECURITY DEFINER
  COST 100;

commit;


CREATE OR REPLACE FUNCTION public.py_get_username(sess_key text )
  RETURNS text
  LANGUAGE plpythonu
AS
$body$
select_plan = plpy.prepare('''select username from profiles where id = (select userid from sessions_current where sess_hash = crypt($1, sess_hash));''', ["text"])
  
result = plpy.execute(select_plan, [sess_key]);
  
return result[0]["username"];
$body$
  VOLATILE
 SECURITY DEFINER
  COST 100;


CREATE OR REPLACE FUNCTION public.py_get_username(sess_key text)
  RETURNS text
  LANGUAGE plpythonu
AS
$body$
select_plan = plpy.prepare('''select username from profiles where id = (select userid from sessions_current where sess_hash = crypt($1, sess_hash));''', ["text"])
  
result = plpy.execute(select_plan, [sess_key]);

if len(result) > 0:
	return result[0]["username"];
else:
	return None;
$body$
  VOLATILE
 SECURITY DEFINER
  COST 100;

COMMIT;

drop function py_user_new(char, char, char);
CREATE OR REPLACE FUNCTION public.py_user_new(username character, password character, email character)
  RETURNS text
  LANGUAGE plpythonu
AS
$body$
check_plan = plpy.prepare('''select count(*) from profiles where username = $1''', ["text"])
	
if (plpy.execute(check_plan, [username])[0]["count"] > 0):
	return "Username already taken"
	

insert_plan = plpy.prepare('''INSERT INTO profiles
    (
      id,
      username,
      passhash,
      email,
      created,
      last_login
    )
    VALUES
    (
      $5,
      $1,
      crypt($2, $3),
      $4,
      current_timestamp,
      current_timestamp
    )''',["text", "text", "text", "text", "int"])
  
next_key = plpy.execute("select NEXTVAL('profiles_id_seq')", 1)[0]["nextval"]
salt = plpy.execute("select gen_salt('bf', 8)", 1)[0]["gen_salt"]
  
plpy.execute(insert_plan, [username, password, salt, email, next_key])
return "Success";
$body$
  VOLATILE
 SECURITY DEFINER
  COST 100;
  
commit;
drop function py_user_check(char,char);
CREATE OR REPLACE FUNCTION public.py_user_check(username character, password character)
  RETURNS text
  LANGUAGE plpythonu
AS
$body$
select_plan = plpy.prepare(
    '''select username from profiles where 
      username = $1 and
      passhash = crypt($2, passhash)''',
    ["text", "text"]
  )
  
id = plpy.execute(select_plan, [username, password])
if (len(id) > 0):
	return id[0]["username"]
else:
	return "Invalid";
$body$
  VOLATILE
 SECURITY DEFINER
  COST 100;

COMMIT;

  
commit;

drop function py_user_check(char, char);
CREATE OR REPLACE FUNCTION public.py_user_check(username character, password character, sess_key char)
  RETURNS bool
  LANGUAGE plpythonu
AS
$body$
select_plan = plpy.prepare(
    '''select id from profiles where 
      username = $1 and
      passhash = crypt($2, passhash)''',
    ["text", "text"]
  )
  
id = plpy.execute(select_plan, [username, password])
if (len(id) > 0):
	update_plan = plpy.prepare('''
		update sessions_current 
			set userid = $1 	
		where sess_hash = crypt($2, sess_hash);''',
		["int", "text"]
	)
	
	plpy.execute(update_plan, [id[0]["id"], sess_key]);

	return True;
else:
	return False;
	
$body$
  VOLATILE
 SECURITY DEFINER
  COST 100;

commit;
ALTER TABLE profiles
   drop CONSTRAINT profiles_email_key;
   
commit;
