DROP VIEW IF EXISTS view_all_sessions CASCADE;

CREATE OR REPLACE VIEW view_all_sessions
(
  userid,
  sessionid,
  ip,
  connect,
  last_updated
)
AS 
 SELECT f.userid,
    f.sessionid,
    f.ip,
    f.connect,
    f.last_updated
   FROM ( SELECT sessions_current.userid,
            sessions_current.sessionid,
            sessions_current.ip,
            sessions_current.connect,
            sessions_current.last_checkin as last_updated
           FROM sessions_current
        UNION
         SELECT sessions_past.userid,
            sessions_past.sessionid,
            sessions_past.ip,
            sessions_past.connect,
            sessions_past.disconnect as last_updated
           FROM sessions_past) f
  ORDER BY f.connect DESC;

GRANT TRUNCATE, TRIGGER, DELETE, REFERENCES, SELECT, INSERT, UPDATE ON view_all_sessions TO sajay;

DROP VIEW IF EXISTS view_usage CASCADE;

CREATE OR REPLACE VIEW view_usage
(
  users,
  requests,
  hour,
  avg_duration
)
AS 
  select count(requests) as users, sum(requests) as requests, min(date_trunc('hour', open_date)) as hour, avg(duration) as avg_duration from 
    (select requests, last_updated - least(open_date, last_updated) as duration, open_date from (select distinct(sessionid), count(sessionid) as requests, min(date_filed) as open_date from log_req group by sessionid order by open_date desc) as req 
    join (select sessionid, ip, last_updated from view_all_sessions) as sess on req.sessionid = sess.sessionid where requests > 1) as a
  group by date_trunc('hour', open_date) order by hour desc;


GRANT TRUNCATE, TRIGGER, DELETE, REFERENCES, SELECT, INSERT, UPDATE ON view_log TO sajay;

commit;
