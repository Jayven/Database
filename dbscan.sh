#Edits files in this files directory. NOT from the callers current directory
#Removes existing explicitly listed files relevent to database
#Restores all files using pg_dump and pg_dumpall
#This is not incremental, but a drop recreate

echo Started $(date) >> lastrun.log

#Stores this files current_director in DIR
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

rm $DIR/jayven-db.sql
pg_dumpall --schema-only --database=jayvengames --file=$DIR/jayven-db.sql --attribute-inserts --disable-dollar-quoting



#Lookup Tables

#rm $DIR/lookup_request
#pg_dump --data-only jayvengames --table=public.lookup_request --file=$DIR/lookup_request

rm $DIR/lookup_detail
pg_dump --data-only jayvengames --table=public.lookup_detail --file=$DIR/lookup_detail

rm $DIR/lookup_tag
pg_dump --data-only jayvengames --table=public.lookup_tag --file=$DIR/lookup_tag

rm $DIR/lookup_theme
pg_dump --data-only jayvengames --table=public.lookup_theme --file=$DIR/lookup_theme

rm $DIR/lookup_status
pg_dump --data-only jayvengames --table=public.lookup_status --file=$DIR/lookup_status

rm $DIR/lookup_component
pg_dump --data-only jayvengames --table=public.lookup_component --file=$DIR/lookup_component


echo Completed $(date) >> lastrun.log
